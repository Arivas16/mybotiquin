package com.example.mybotiquin.mybotiquin;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

/**
 * Created by annerys on 20/03/15.
 */
public class RegistrarUsuario extends Activity{
    EditText nombreusuario = null;
    EditText correousuario  = null;
    EditText claveusuario = null;
    EditText claveusuario2 = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registrar_usuario);
        nombreusuario = (EditText)findViewById(R.id.nombreRegistrar);
        correousuario = (EditText)findViewById(R.id.correoRegistrar);
        claveusuario = (EditText)findViewById(R.id.claveRegistrar);
        claveusuario2 = (EditText)findViewById(R.id.clave2Registrar);
    }

    private class Registrar extends AsyncTask<String, Void, String> {

        protected String doInBackground(String... urls){
            Log.i("ANtes de llamar al api soyy la url", urls[0]);
            return Util.Registrar(urls[0], nombreusuario.getText().toString(), correousuario.getText().toString(), claveusuario.getText().toString());
        }

        protected void onPostExecute(String result){
            try{
                JSONObject jsonObject = new JSONObject(result);
                //String token = jsonObject.getString("token").toString();
                //Util.getInstance().setToken(token);
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                //Si quisieramos pasar parámetros
                //intent.putExtra("clave", "valor");
                //intent.putExtra("token", token);
                startActivity(intent);

                JSONObject jsonObj = new JSONObject(result);
                Toast mensaje_registro;
                if( jsonObj.getString("nombre").equals("Correo existente")){
                    mensaje_registro = Toast.makeText(getBaseContext(), "El correo ya existe, por favor intentelo nuevamente", Toast.LENGTH_SHORT);
                }else{
                    if( jsonObj.getString("nombre").equals("Usuario no creado")){
                        mensaje_registro = Toast.makeText(getBaseContext(), "Usuario no registrado, por favor intentelo nuevamente", Toast.LENGTH_SHORT);
                    }else{
                         mensaje_registro = Toast.makeText(getBaseContext(), "Usuario registrado, por favor inicie sesion", Toast.LENGTH_SHORT);
                    }
                }
                mensaje_registro.setGravity(Gravity.TOP,0,400);
                mensaje_registro.show();

            } catch (Exception e){
                //error_message.setText("Invalid login or password");
                Toast mensaje = Toast.makeText(getBaseContext(), "ALgo paso", Toast.LENGTH_SHORT);
                mensaje.setGravity(Gravity.TOP,0,400);
                mensaje.show();
            }
        }
    }

    public void pruebaApi (View view){
        System.out.println("entre a pruebaapi");
        new Registrar().execute("http://192.168.1.103:3000/usuarios/crear");
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
    }
}
