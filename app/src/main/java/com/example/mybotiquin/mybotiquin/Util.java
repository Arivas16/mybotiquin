package com.example.mybotiquin.mybotiquin;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import org.json.JSONArray;
import java.util.List;


/**
 * Created by annerys on 20/03/15.
 */



public class Util {

    private String token = null;
    private Integer idGlobalUsuario= null;
    private static Util instance = null;
    private Integer idLocalUsuario= null;

    public static String Registrar(String url,String nombre, String correo, String clave){
        Log.i("entre al registrar con la url", url);
        StringBuilder stringBuilder = new StringBuilder();
        try{
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);
            List<NameValuePair> parameters = new ArrayList<NameValuePair>(3);
            parameters.add(new BasicNameValuePair("nombre", nombre));
            parameters.add(new BasicNameValuePair("clave", clave));
            parameters.add(new BasicNameValuePair("correo", correo));
            httpPost.setEntity(new UrlEncodedFormEntity(parameters));
            HttpResponse response = httpClient.execute(httpPost);
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode == 200) {
                HttpEntity httpEntity = response.getEntity();
                //Leemos la entrada y convertimos el json en un string
                InputStream inputStream = httpEntity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                String line;
                while ((line = reader.readLine()) != null) {
                    stringBuilder.append(line);
                }
                inputStream.close();

                return stringBuilder.toString();
            } else {
                Log.i("status", "ERROR");
                return "error";
            }
        }catch (Exception e){
            Log.i("status", "ERROR INTENTANDO CONECTAR CON EL API y " + e.getLocalizedMessage());
            return "error en la conexion con el api";
        }
    }

    public String getToken() {
        return token;
    }

    public Integer getIdGlobalUsuario() {
        return idGlobalUsuario;
    }
    public void setIdGlobalUsuario(Integer idglobalusuario) {
        this.idGlobalUsuario = idglobalusuario;
    }

    public Integer getIdLocalUsuario() {
        return idLocalUsuario;
    }
    public void setIdLocalUsuario(Integer idlocalusuario) {
        this.idLocalUsuario = idlocalusuario;
    }


    public static Util getInstance( ) {
        if (instance == null)
            instance = new Util();
        return instance;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public static String Iniciar(String url,String correo, String clave){
        Log.i("entre al iniciar sesion", url);
        StringBuilder stringBuilder = new StringBuilder();
        try{
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);
            List<NameValuePair> parameters = new ArrayList<NameValuePair>(2);
            parameters.add(new BasicNameValuePair("clave", clave));
            parameters.add(new BasicNameValuePair("correo", correo));
            httpPost.setEntity(new UrlEncodedFormEntity(parameters));
            HttpResponse response = httpClient.execute(httpPost);
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode == 200) {
                HttpEntity httpEntity = response.getEntity();
                //Leemos la entrada y convertimos el json en un string
                InputStream inputStream = httpEntity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                String line;
                while ((line = reader.readLine()) != null) {
                    stringBuilder.append(line);
                }
                inputStream.close();
                Log.i("resultado en string", stringBuilder.toString());
                return stringBuilder.toString();
            } else {
                Log.i("status", "ERROR");
                return "error";
            }
        }catch (Exception e){
            Log.i("status", "ERROR INTENTANDO CONECTAR CON EL API y " + e.getLocalizedMessage());
            return "error en la conexion con el api";
        }
    }

    public static String ListarTratamientos(String url){
        Log.i("entre a listar tratamientos", url);
        StringBuilder stringBuilder = new StringBuilder();
        try{
            HttpClient httpClient = new DefaultHttpClient();
            HttpGet httpget = new HttpGet(url);
            HttpResponse response = httpClient.execute(httpget);
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode == 200) {
                HttpEntity httpEntity = response.getEntity();
                //Leemos la entrada y convertimos el json en un string
                InputStream inputStream = httpEntity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                String line;
                while ((line = reader.readLine()) != null) {
                    stringBuilder.append(line);
                }
                inputStream.close();
                Log.i("resultado en string", stringBuilder.toString());
                return stringBuilder.toString();
            } else {
                Log.i("status", "ERROR");
                return "error";
            }
        }catch (Exception e){
            Log.i("status", "ERROR INTENTANDO CONECTAR CON EL API y " + e.getLocalizedMessage());
            return "error en la conexion con el api";
        }
    }

}

