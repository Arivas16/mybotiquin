package com.example.mybotiquin.mybotiquin;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

public class IniciarSesion  extends Activity {

    EditText correoIniciarSesion = null;
    EditText claveIniciarSesion  = null;

    DB basededatos = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.iniciar_sesion);
        correoIniciarSesion = (EditText)findViewById(R.id.correoIniciarSesion);
        claveIniciarSesion = (EditText)findViewById(R.id.claveIniciarSesion);
        basededatos = new DB(this);

    }




    private class ejecutarIniciar extends AsyncTask<String, Void, String> {

        protected String doInBackground(String... urls){

            return Util.Iniciar(urls[0], correoIniciarSesion.getText().toString(), claveIniciarSesion.getText().toString());
        }

        protected void onPostExecute(String result){
            try{
                JSONObject jsonObject = new JSONObject(result);
                Toast mensaje_iniciar;
                if (!(jsonObject.getString("nombre").equals("Error en los datos"))){
                    String token = jsonObject.getString("token");
                    Util.getInstance().setToken(token);
                    Integer idglobal = jsonObject.getInt("id_usuario");
                    Util.getInstance().setIdGlobalUsuario(idglobal);

                    try{
                        basededatos.abrir_bd();
                    } catch (Exception e){
                        Log.d("ERROR", "ERROR CONEXION BD");
                    }
                    basededatos.limpiarBD();
                    basededatos.crearUsuario(jsonObject.getString("clave"),jsonObject.getString("token"), jsonObject.getString("correo"),jsonObject.getInt("id_usuario"));
                    try{
                        basededatos.cerrar_bd();
                    } catch (Exception e){
                        Log.d("ERROR", "ERROR CLOSE BD");
                    }

                    mensaje_iniciar = Toast.makeText(getBaseContext(), "Inicio sesion y en teoría guardó al usuario en la bd local", Toast.LENGTH_SHORT);
                    LlamarListarTratamiento();
                }else{
                    mensaje_iniciar = Toast.makeText(getBaseContext(), "Ocurrio un error con sus datos, por favor intentelo de nuevo", Toast.LENGTH_SHORT);
                }

                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                mensaje_iniciar.setGravity(Gravity.TOP,0,400);
                mensaje_iniciar.show();
            } catch (Exception e){
                //error_message.setText("Invalid login or password");
                Log.i("status", "ERROR EN ALGUNA PARTE CON ESTO " + e.getLocalizedMessage());
                Log.i("status", "algooo paso aquiii" + e.getMessage());
                Toast mensaje_iniciar;
                mensaje_iniciar = Toast.makeText(getBaseContext(), "ALgo paso en el iniciar sesion", Toast.LENGTH_SHORT);
                mensaje_iniciar.setGravity(Gravity.TOP,0,400);
                mensaje_iniciar.show();
            }
        }
    }

    public void LlamarIniciar (View view){
        System.out.println("entre a iniciar");
        new ejecutarIniciar().execute("http://192.168.1.103:3000/usuarios/autenticar");
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
    }

    private class listarTrats extends AsyncTask<String, Void, String> {

        protected String doInBackground(String... urls){
            return Util.ListarTratamientos(urls[0]);
        }

        protected void onPostExecute(String result){
            System.out.println("entre a onpostexcecute");
            try{
                System.out.println("entre al try");
                long id_tratamiento_local;

                try{
                    basededatos.abrir_bd();
                } catch (Exception e) {
                    Log.d("ERROR", "ERROR CONEXION BD");
                }

                JSONArray jsonArray = new JSONArray(result);
                System.out.println("el tamaño de jsonarrya es "+jsonArray.length());
                for (Integer i=0; i<jsonArray.length(); i++){
                    System.out.println("entre al for ");
                    JSONObject c = jsonArray.getJSONObject(i);
                    System.out.println("el nombre del tratamiento a crear es "+c.getString("nombre"));
                    System.out.println("el nombre_medico del tratamiento a crear es "+c.getString("nombre_medico"));
                    System.out.println("la fechatrata del tratamiento a crear es "+c.getString("fechatrata"));
                    System.out.println("el lunes del tratamiento a crear es "+c.getString("lunes"));
                    System.out.println("el martes del tratamiento a crear es "+c.getString("martes"));
                    System.out.println("el miercoles del tratamiento a crear es "+c.getString("miercoles"));
                    System.out.println("el jueves del tratamiento a crear es "+c.getString("jueves"));
                    System.out.println("el viernes del tratamiento a crear es "+c.getString("viernes"));
                    System.out.println("el sabado del tratamiento a crear es "+c.getString("sabado"));
                    System.out.println("el domingo del tratamiento a crear es "+c.getString("domingo"));
                    System.out.println("los dias del tratamiento a crear es "+c.getString("dias"));
                    System.out.println("el contador del tratamiento a crear es "+c.getString("contador"));
                    System.out.println("el contado_at del tratamiento a crear es "+c.getString("contado_at"));
                    System.out.println("la fechaultimamodificacion del tratamiento a crear es "+c.getString("fecha_ultima_modificacion"));
                    System.out.println("el idverdadero del tratamiento a crear es "+c.getString("id_tratamiento"));
                    System.out.println("el borrado del tratamiento a crear es "+c.getString("borrado"));
                    id_tratamiento_local = basededatos.crearTratamiento(c.getString("nombre"), c.getString("nombre_medico"), c.getString("fechatrata") ,Util.getInstance().getIdLocalUsuario() ,c.getString("lunes") , c.getString("martes"), c.getString("miercoles"), c.getString("jueves") ,c.getString("viernes"),c.getString("sabado"), c.getString("domingo"), c.getString("dias"), c.getString("contador"), c.getString("contado_at"), c.getString("fecha_ultima_modificacion"),c.getString("id_tratamiento"), c.getString("borrado"));
                    System.out.println("el id del tratamiento creado es "+id_tratamiento_local);
                    JSONArray r = c.getJSONArray("recordatorios");
                    System.out.println("el tamaño del arreglo de recordatorios es "+r.length());
                    for (Integer j=0; j<r.length();j++){
                        System.out.println("entre al for de los recordatorios");
                        JSONObject arreglo_recordatorio = r.getJSONObject(j);
                        //basededatos.crearRecordatorio(arreglo_recordatorio.getString("hora"), arreglo_recordatorio.getString("tomado") , arreglo_recordatorio.getString("tomado_at") , id_tratamiento_local, arreglo_recordatorio.getString("fecha_ultima_modificacion") , arreglo_recordatorio.getInt("id_verdadero") ,arreglo_recordatorio.getInt("borrado"));
                    }
                }

                basededatos.listarUsuario();
                basededatos.listarTratamientos();
                basededatos.listarRecordatorios();

                try{
                    basededatos.cerrar_bd();
                } catch (Exception e){
                    Log.d("ERROR", "ERROR CLOSE BD");
                }
            } catch (Exception e){
                //error_message.setText("Invalid login or password");
                System.out.println("entre a una ecepcion en el crear tratamientos");
                Toast mensaje_iniciar;
                mensaje_iniciar = Toast.makeText(getBaseContext(), "ALgo paso en el listar tratamiento", Toast.LENGTH_SHORT);
                mensaje_iniciar.setGravity(Gravity.TOP,0,400);
                mensaje_iniciar.show();
            }
        }
    }

    public void LlamarListarTratamiento (){
        System.out.println("entre al listar tratamientor");
        new listarTrats().execute("http://192.168.1.103:3000/tratamientos/listar_recordatorio/"+Util.getInstance().getIdGlobalUsuario()+"/"+Util.getInstance().getToken());
    }



}
