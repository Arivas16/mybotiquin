package com.example.mybotiquin.mybotiquin;

/**
 * Created by annerys on 27/03/15.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DB extends SQLiteOpenHelper {

    private SQLiteDatabase database;

    public DB(Context context) {
        super(context, "appmoviles", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String create_table_usuario = "CREATE TABLE IF NOT EXISTS usuario (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, clave text , token text, correo text, id_verdadero integer)";
        String create_table_tratamiento = "CREATE TABLE IF NOT EXISTS tratamiento (id_tratamiento INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, nombre text , nombre_medico text, fechatrata text, usuario_id integer, lunes integer, martes integer, miercoles integer , jueves integer, viernes integer, sabado integer, domingo integer, dias integer, contador integer, contado_at integer, fecha_ultima_modificacion text, id_verdadero integer, borrado integer)";
        String create_table_recordatorio = "CREATE TABLE IF NOT EXISTS recordatorio (id_recordatorio INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL , hora text, tomado integer, tomado_at text, tratamiento_id integer, fecha_ultima_modificacion text, id_verdadero integer, borrado integer)";
        db.execSQL(create_table_usuario);
        db.execSQL(create_table_tratamiento);
        db.execSQL(create_table_recordatorio);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        return;
    }

    public void abrir_bd() throws SQLException{
        database = this.getWritableDatabase();
    }

    public void cerrar_bd(){
        this.close();
    }

    public void crearUsuario(String clave, String token, String correo,Integer id_verdadero){
        //System.out.println("entre a crear el usuario con "+clave+" "+token+" "+correo+" "+id_verdadero);
        String[] columns = {"id", "clave", "token" , "correo" ,"id_verdadero"};
        ContentValues contentValues = new ContentValues();
        contentValues.put("clave",clave );
        contentValues.put("token",token );
        contentValues.put("correo",correo );
        contentValues.put("id_verdadero",id_verdadero );
        Integer insertId = (int) database.insert("usuario",null,contentValues);
        Util.getInstance().setIdLocalUsuario(insertId);
    }

    public long crearTratamiento(String nombre ,String nombre_medico, String fechatrata ,Integer usuario_id,String lunes, String martes ,String miercoles ,String jueves,String viernes , String sabado ,String domingo, String dias, String contador,String contado_at, String fecha_ultima_modificacion , String id_verdadero ,String borrado){

        System.out.println("voy a crear un tratamiento");
        String[] columns = { "id_tratamiento","nombre", "nombre_medico", "fechatrata" , "usuario_id" ,"lunes", "martes", "miercoles", "jueves" ,"viernes","sabado", "domingo", "dias", "contador", "contado_at", "fecha_ultima_modificacion","id_verdadero", "borrado" };
        ContentValues contentValues = new ContentValues();
        contentValues.put("nombre",nombre );
        contentValues.put("nombre_medico",nombre_medico );
        contentValues.put("fechatrata",fechatrata );
        contentValues.put("usuario_id",usuario_id );
        contentValues.put("lunes", StringtoInt(lunes) );
        contentValues.put("martes", StringtoInt(martes) );
        contentValues.put("miercoles", StringtoInt(miercoles) );
        contentValues.put("jueves", StringtoInt(jueves) );
        contentValues.put("viernes", StringtoInt(viernes) );
        contentValues.put("sabado", StringtoInt(sabado) );
        contentValues.put("domingo", StringtoInt(domingo) );
        contentValues.put("contador", Integer.parseInt(contador) );
        contentValues.put("contado_at", VaciotoNull(contado_at) );
        contentValues.put("fecha_ultima_modificacion", VaciotoNull(fecha_ultima_modificacion) );
        contentValues.put("id_verdadero", Integer.parseInt(id_verdadero) );
        contentValues.put("borrado", StringtoInt(borrado) );
        long insertId =  database.insert("tratamiento",null,contentValues);
        System.out.println("soy el id del tratamiento local creado"+ insertId);
        return insertId;
    }

    public Integer StringtoInt (String dato){
        if (dato.equals("true")){
            return 1;
        }else{
            return 0;
        }
    }

    public String VaciotoNull (String dato){
        if (dato.equals("") || dato.equals(" ")){
            return null;
        }else{
            return dato;
        }
    }

    public void crearRecordatorio (String hora, Integer tomado , String tomado_at , long tratamiento_id, String fecha_ultima_modificacion , Integer id_verdadero , Integer borrado){
        System.out.println("voy a crear un recordatorio");
        String[] columns = { "id_recordatorio", "hora", "tomado" , "tomado_at" , "tratamiento_id", "fecha_ultima_modificacion" , "id_verdadero" ,"borrado" };
        ContentValues contentValues = new ContentValues();
        contentValues.put("hora", hora);
        contentValues.put("tomado", tomado);
        contentValues.put("tomado_at", tomado_at);
        contentValues.put("tratamiento_id", tratamiento_id);
        contentValues.put("fecha_ultima_modificaicon", fecha_ultima_modificacion);
        contentValues.put("id_verdadero", id_verdadero);
        contentValues.put("borrado", borrado);
        long insertId =  database.insert("recordatorio",null,contentValues);
       // System.out.println("soy el id del recordatorio local creado"+ insertId);
    }

    public void listarUsuario (){
        System.out.println("VOY A LISTAR UN USUARIO");
        String[] columns = {"id", "correo"};
        Cursor cursor = database.query("usuario",columns,null,null,null,null,null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()){
            System.out.println("soy el id del usuario en la BD local "+cursor.getLong(0));
            System.out.println("soy el correo del usuario en la Bd local "+cursor.getString(1));
            cursor.moveToNext();
        }
        cursor.close();
    }

    public void listarTratamientos (){
        System.out.println("VOY LISTAR TRATAMIENTO ");
        String[] columns = {"id_tratamiento","nombre", "nombre_medico", "fechatrata" , "usuario_id" ,"lunes", "martes", "miercoles", "jueves" ,"viernes","sabado", "domingo", "dias", "contador", "contado_at", "fecha_ultima_modificacion","id_verdadero", "borrado" };
        Cursor cursor = database.query("tratamiento",columns,null,null,null,null,null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()){
            System.out.println("soy el id del tratamiento en la BD local "+cursor.getLong(0));
            System.out.println("soy el nombre del tratamiento en la BD local "+cursor.getString(1));
            System.out.println("el nombre_medico del tratamiento en la BD local "+cursor.getString(2));
            System.out.println("la fechatrata del tratamiento en la BD local "+cursor.getString(3));
            System.out.println("el lunes del tratamiento en la BD local"+cursor.getString(4));
            System.out.println("el martes del tratamiento en la BD local"+cursor.getString(5));
            System.out.println("el miercoles del tratamiento en la BD local"+cursor.getString(6));
            System.out.println("el jueves del tratamiento en la BD local"+cursor.getString(7));
            System.out.println("el viernes del tratamiento en la BD local"+cursor.getString(8));
            System.out.println("el sabado del tratamiento en la BD local"+cursor.getString(9));
            System.out.println("el domingo del tratamiento en la BD local"+cursor.getString(10));
            System.out.println("los dias del tratamiento en la BD local"+cursor.getLong(11));
            System.out.println("el contador del tratamiento en la BD local"+cursor.getLong(12));
            System.out.println("el contado_at del tratamiento en la BD local"+cursor.getLong(13));
            System.out.println("la fechaultimamodificacion del tratamiento en la BD local"+cursor.getString(14));
            System.out.println("el idverdadero del tratamiento en la BD local"+cursor.getLong(15));
            System.out.println("el borrado del tratamiento aen la BD local"+cursor.getString(16));
            cursor.moveToNext();
        }
        cursor.close();
    }

    public void listarRecordatorios (){
        System.out.println("VOY A LISTAR RECORDATORIO ");
        String[] columns = {"id_recordatorio", "hora", "tratamiento_id"};
        Cursor cursor = database.query("tratamiento",columns,null,null,null,null,null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()){
            System.out.println("soy el id del recordatorio en la BD local "+cursor.getLong(0));
            System.out.println("soy la hora del recordatorio en la BD local "+cursor.getString(1));
            System.out.println("soy el id del tratamiento al q esta asociado el recordatorio en la BD local "+cursor.getLong(2));
            cursor.moveToNext();
        }
        cursor.close();
    }

    public void limpiarBD (){
        database.delete("usuario", null, null);
        database.delete("tratamiento", null, null);
        database.delete("recordatorio", null, null);
    }
}
